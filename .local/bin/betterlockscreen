#!/usr/bin/env bash

# Author : Pavan Jadhaw
# Github Profile : https://github.com/pavanjadhaw
# Project Repository : https://github.com/pavanjadhaw/betterlockscreen
# modified by spkry
# removes wallpaper changer, replaces one convert command with equivalent ffmpeg command,
# instead of text the date is converted into a clock shadow to avoid creating a gray box,
# as well as some other, mostly aestetic changes. as a result, the script is much faster.

# find your resolution so images can be resized to match your screen resolution
res=$(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')
locktext='Type password to unlock...'


init_filenames() {
	#$1 resolution

	# custom i3lock colors
	theme_rc="$HOME/.config/betterlockscreenrc"
	if [ -e $theme_rc ]; then
	    source "$theme_rc"
	else
        # copy this block to ~/.config/betterlockscreenrc" to customize
	insidecolor=00000000
	ringcolor=ffffff00
	keyhlcolor=00000000
	bshlcolor=00000000
	separatorcolor=00000000
	insidevercolor=bbbbbbaa
	insidewrongcolor=bb0000aa
	ringvercolor=00000000
	ringwrongcolor=00000000
	verifcolor=ffffffff
	timecolor=000000aa
	datecolor=ffffffff
	loginbox=00000066
	lock_timeout=1
	font=Terminus
	fontsize=28
	sfontsize=29
	fi

	# create folder in ~/.cache/i3lock directory
	res_folder="$HOME/.cache/i3lock/$1"
	folder="$HOME/.cache/i3lock/current"
	echo "Got" $@ $res_folder
	if [ ! -d $folder -o -n "$2" ]; then
		rm -f $folder
		ln -s $res_folder $folder
	fi

	# ratio for rectangle to be drawn for time background on lockscreen
	# Original Image
	orig_wall="$folder/wall.png"

	# Versions (from here)
	# You can use these images to set different versions as wallpaper
	# lockscreen background.
	resized="$folder/resized.png" # resized image for your resolution

	# images to be used as wallpaper
	dim="$folder/dim.png" # image with subtle overlay of black
	blur="$folder/blur.png" # blurred version
	dimblur="$folder/dimblur.png"

	# lockscreen images (images to be used as lockscreen background)
	l_resized="$folder/l_resized.png"
	l_dim="$folder/l_dim.png"
	l_blur="$folder/l_blur.png"
	l_dimblur="$folder/l_dimblur.png"
}

init_filenames $res


prelock() {
	if [ ! -z "$(pidof dunst)" ]; then
		pkill -u "$USER" -USR1 dunst
	fi
}


lock() {
	#$1 image path

	i3lock \
		-i "$1" -C \
		--time-pos='x+46:h-46' --date-pos="tx-2:ty-2" \
		--time-str="%H:%M" --time-align=1 --date-align=1 \
		--clock --date-str="%H:%M" --date-color="$datecolor" \
		--inside-color="$insidecolor" --ring-color="$ringcolor" --line-uses-inside \
		--keyhl-color="$keyhlcolor" --bshl-color="$bshlcolor" --separator-color="$separatorcolor" \
		--insidever-color="$insidevercolor" --insidewrong-color="$insidewrongcolor" \
		--ringver-color="$ringvercolor" --ringwrong-color="$ringwrongcolor" --ind-pos='x+200:h-57' \
		--radius=4 --ring-width=1 --verif-text='' --wrong-text='' \
		--verif-color="$verifcolor" --time-color="$timecolor"  \
		--time-font="$font"  --date-font="$font" \
		--time-size="$fontsize" --date-size="$fontsize" \
		--noinput-text='' --force-clock $lockargs
}


postlock() {
	if [ ! -z "$(pidof dunst)" ] ; then
		pkill -u "$USER" -USR2 dunst
	fi
}


rec_get_random() {
	dir="$1"
	if [ ! -d "$dir" ]; then
		user_input="$dir"
		return
	fi
	dir=("$dir"/*)
	dir="${dir[RANDOM % ${#dir[@]}]}"
	rec_get_random "$dir"
}


lockselect() {
	prelock
	case "$1" in
		dim)
			# lockscreen with dimmed background
			lock "$l_dim"
			;;

		blur)
			# set lockscreen with blurred background
			lock "$l_blur"
			;;


		*)
			# default lockscreen
			lock "$l_resized"
			;;
	esac
	postlock
}

logical_px() {
	# get dpi value from xrdb
	local DPI=$(xrdb -query | awk '/Xft.dpi/ {print $2}')
	
	# return the default value if no DPI is set
	if [ -z "$DPI" ]; then
		echo $1
	else
		local SCALE=$(echo "scale=2; $DPI / 96.0" | bc)

		# check if scaling the value is worthy
		if [ $(echo "$SCALE > 1.25" | bc -l) -eq 0 ]; then
			echo $1
		else
			echo "$SCALE * $1 / 1" | bc
		fi
	fi
}

update() {
	# use 
	background="$1"

	# default blur level; fallback to 1
	[[ $blur_level ]] || blur_level=1

	rectangles=" "
	#SR=$(xrandr --query | grep ' connected' | grep -o '[0-9][0-9]*x[0-9][0-9]*[^ ]*')
	#for RES in $SR; do
	#	SRA=(${RES//[x+]/ })
	#	CX=$((${SRA[2]} + $(logical_px 25)))
	#	CY=$((${SRA[1]} - $(logical_px 30)))
	#	rectangles+="rectangle $CX,$CY $((CX+$(logical_px 300))),$((CY-$(logical_px 80))) "
	#done

	# User supplied Image
	user_image="$folder/user_image.png"

	# create folder
	if [ ! -d $folder ]; then
		echo "Creating '$folder' directory to cache processed images."
		mkdir -p "$folder"
	fi

	# get random file in dir if passed argument is a dir
	rec_get_random "$background"

	# get user image
	cp "$user_input" "$user_image"
	if [ ! -f $user_image ]; then
		echo 'Please specify the path to the image you would like to use'
		exit 1
	fi

	# replace orignal with user image
	cp "$user_image" "$orig_wall"
	rm "$user_image"

	echo 'Generating alternate images based on the image you specified,'
	echo 'please wait this might take few seconds...'

	# wallpapers

	echo
	echo 'Converting provided image to match your resolution...'
	# resize image
	#convert "$orig_wall" -resize "$res""^" -gravity center -extent "$res" "$l_resized"
	ffmpeg -y -i "$orig_wall" -vf "scale=${res/x/:}:force_original_aspect_ratio=increase,crop=${res/x/:}" -qscale:v 2 -loglevel quiet "$l_resized"

	#cp "$orig_wall" "$l_resized"
	echo
	echo 'Applying dim and blur effect to resized image'

	# blur
	blur_shrink=$(echo "scale=2; 8 / $blur_level" | bc)
	blur_sigma=$(echo "scale=2; 0.6 * $blur_level" | bc)
	convert "$l_resized" \
		-filter Gaussian \
		-resize "$blur_shrink%" \
		-define "filter:sigma=$blur_sigma" \
		-resize "$res^" -gravity center -extent "$res" \
		"$l_blur"
	#convert \
	#	-resize "$blur_shrink%" \
	#	-blur "0x$blur_sigma" \
	#	-resize "$res^" -gravity center -extent "$res" \
	#	"$l_resized" "$l_blur"
	
# ffmpeg -i "$l_resized" -filter:a gblur=$blur_sigma "$l_blur"

	echo
	echo 'All required changes have been applied'
}



empty() {
	if [ -f $l_dim ]; then
		echo -e "\nSeems you haven't provided any arguments. See below for usage details."
	else
		echo 'Important: Update the image cache (e.g. betterlockscreen -u path/to/image.jpg).'
		echo
		echo '		Image cache must be updated to initially configure or update the wallpaper used.'
	fi

	echo
	echo 'For other sets of options and help, use the help command.'
	echo 'e.g. betterlockscreen -h or betterlockscreen --help'
	echo
	echo 'See: https://github.com/pavanjadhaw/betterlockscreen for additional info...'
	exit 1
}


usage() {
	echo 'Important: Update the image cache (e.g. betterlockscreen -u path/to/image.jpg).'
	echo '	Image cache must be updated to initially configure or update wallpaper used'
	echo
	echo
	echo 'See: https://github.com/pavanjadhaw/betterlockscreen for additional info...'
	echo
	echo
	echo 'Options:'
	echo
	echo '	-h --help'
	echo '		For help (e.g. betterlockscreen -h or betterlockscreen --help).'
	echo
	echo
	echo '	-u --update'
	echo '		to update image cache, you should do this before using any other options'
	echo '		E.g: betterlockscreen -u path/to/image.png when image.png is custom background'
	echo '		Or you can use betterlockscreen -u path/to/imagedir and a random file will be selected.'
	echo
	echo
	echo '	-l --lock'
	echo '		to lock screen (e.g. betterlockscreen -l)'
	echo '		you can also use dimmed or blurred background for lockscreen.'
	echo '		E.g: betterlockscreen -l dim (for dimmed background)'
	echo '		E.g: betterlockscreen -l blur (for blurred background)'
	echo '		E.g: betterlockscreen -l dimblur (for dimmed + blurred background)'
	echo
	echo
	echo '	-s --suspend'
	echo '		to suspend system and lock screen (e.g. betterlockscreen -s)'
	echo '		you can also use dimmed or blurred background for lockscreen.'
	echo '		E.g: betterlockscreen -s dim (for dimmed background)'
	echo '		E.g: betterlockscreen -s blur (for blurred background)'
	echo '		E.g: betterlockscreen -s dimblur (for dimmed + blurred background)'
	echo
	echo
	echo '	-r --resolution'
	echo '		to be used after -u'
	echo '		used to set a custom resolution for the image cache.'
	echo '		E.g: betterlockscreen -u path/to/image.png -r 1920x1080'
	echo '		E.g: betterlockscreen -u path/to/image.png --resolution 3840x1080'
	echo
	echo
	echo '	-b --blur'
	echo '		to be used after -u'
	echo '		used to set blur intensity. Default to 1.'
	echo '		E.g: betterlockscreen -u path/to/image.png -b 3'
	echo '		E.g: betterlockscreen -u path/to/image.png --blur 0.5'
	echo
	echo
	echo '	-t --text'
	echo '		to set custom lockscreen text (max 31 chars)'
	echo "		E.g: betterlockscreen -l dim -t \"Don't touch my machine!\""
	echo '		E.g: betterlockscreen --text "Hi, user!" -s blur'
}


# Options
[[ "$1" = '' ]] && empty

for arg in "$@"; do
	[[ "${arg:0:1}" = '-' ]] || continue

	case "$1" in
		-h | --help)
			usage
			break
			;;

		-s | --suspend)
			runsuspend=true
			;&

		-l | --lock)
			runlock=true
			[[ $runsuspend ]] || lockargs="$lockargs -n"
			[[ ${2:0:1} = '-' ]] && shift 1 || { lockstyle="$2"; shift 2; }
			;;

		-u | --update)
			runupdate=true
			imagepath="$2"
			shift 2
			;;

		-t | --text)
			locktext="$2"
			shift 2
			;;

		-r | --resolution)
			res="$2"
			init_filenames $res force
			shift 2
			;;

		-b | --blur)
			blur_level="$2"
			shift 2
			;;

		--)
			lockargs="$lockargs ${@:2}"
			break
			;;

		*)
			echo "invalid argument: $1"
			break
			;;
	esac
done

# Run image generation
[[ $runupdate ]] && update "$imagepath"

# Activate lockscreen
[[ $runlock ]] && lockselect "$lockstyle" && \
	{ [[ $runsuspend ]] && systemctl suspend; }

exit 0
