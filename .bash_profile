# /etc/skel/.bash_profile
cat "$HOME/banner.ssh"

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.

if [[ -f ~/.bashrc ]] ; then
	. ~/.bashrc
fi
export PATH="${PATH}:/home/spkry/.local/bin"
export EDITOR="vim"
export GTK_IM_MODULE='fcitx'
export QT_IM_MODULE='fcitx'
export SDL_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'
if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep dwm || startx
fi
shortcuts >/dev/null 2>&1 
