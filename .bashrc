# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# gentoo-style PS1, i like it a lot
#export PS1="\[$(tput bold)\]\[\033[38;5;10m\]\u@\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;12m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;12m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"
# new ps1
export PS1="\[\033[38;5;38m\]\w\[$(tput sgr0)\]\n\[$(tput sgr0)\]\[\033[38;5;38m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]"
# Put your fun stuff here.
# git stuff
alias gdf="/usr/bin/git --git-dir=/home/spkry/dotfiles --work-tree=/home/spkry"

export EDITOR="/usr/bin/vim"
export term="/usr/bin/alacritty"
export brwsr="/usr/bin/firefox"
export PATH="/home/spkry/.local/bin/blocks:/usr/bin/grabber:$PATH"

# more important aliases
alias recomp="rm config.h && make && sudo make clean install"
alias ls="ls -Al --color"
alias du="du -d 1 -h"
alias mv="mv -v"
alias mpv="devour mpv"
alias grabber="devour Grabber"
alias sxiv="devour sxiv -ba -s h"
alias mupdf="devour mupdf"
alias tremc="tremc -c 127.0.0.1:49155"
alias ytdl="youtube-dl"
alias hsb="sudo headsetcontrol -cb"
alias tcryptsetup="sudo cryptsetup --type tcrypt --veracrypt open"
alias wttr="curl wttr.in/Riga"
alias getip="curl ifconfig.co"
alias bright="sudo /home/spkry/.local/bin/brightness"
alias corona="curl https://corona-stats.online/lv"
alias pfetch='USER="desu" PF_INFO="ascii title os kernel wm" pfetch'

#links
alias mybin="cd ~/.local/bin"
alias cfg="cd ~/.config"
alias media="cd /media"

# just random shit
alias rr="curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash"
